PS1="%F{51}[%n@%m]%f %F{130}%~ >>%f "
EDITOR="vim"
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)
export LS_COLORS="di=4;36:fi=35:ln=34;47:or=31;44:ex=93"
TERMINAL="gnome-terminal"
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zshistory

export XDG_CONFIG_HOME=/home/clarity/.config
alias ll="ls -lah --color=always --group-directories-first"
alias ip="ip -c"
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh >/dev/null
